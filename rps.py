'''
RockPaperScissors game against computer
Rock wins scissors
Scissors wins paper
Paper wins Rock

Approach:
1. Import three options for game: Rock, Paper, Scissors
2. Randomly select rock/paper/scissor for computer
3. Create function with three possible outcomes - Win/Lose/Tie
'''
import random
randChoice = (['Paper', 'Scissors', 'Rock'])
hand = random.choice(randChoice).capitalize()

def rps(hand, userChoice):
    if((userChoice == 'Rock' and hand == "Scissors") or (userChoice == 'Paper' and hand == "Rock") or (userChoice == 'Scissor' and hand == 'Paper')):
        print("You Win! Nice try computer.")
    elif((userChoice =='Rock' and hand == 'Paper') or (userChoice == 'Paper' and hand == 'Scissors') or (userChoice == 'Scissors' and hand == 'Rock')):
        print('Computer Wins! Good try, player')
    elif (userChoice == hand):
        print('Tie Game ')
    
userChoice = input('Lets play Rock Paper Scissors!\nPlease pick your choice \n-Rock, Paper, Scissors: ').capitalize()
print('Player\"s choice ' + str(userChoice) +  "\nComputer's choice: " +str(hand))
rps(hand, userChoice)
print("lets play")