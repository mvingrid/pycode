'''
Description: Declare two emply lists, one for the name and one for the salaries. Within the for loop
ask for employees name and their salaries and append them into the list accordingly.
find out the total salary using accumulation concept within the for loop.
'''
names = []
salaries = []
sumsal = 0

output = int(input("How many employees would you like to record: "))

for i in range(0,output):
    name = input("Please enter employee name: ").capitalize()
    names.append(name)
    salary = float(input("Please enter salary: "))
    salaries.append(salary)
    sumsal = sumsal + salary
print("The following are the employee names and their salary, respectively: ", names, salaries)
print("The calculated total salaries are: ", sumsal)