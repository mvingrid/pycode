'''
Define function is_palindrome(), recognize if word is
palindrome. Words that look the same written backwards
'''

def is_palindrome(userinput):
    ispali = ""
    for n in range(len(userinput)-1,-1,-1):
        ispali += userinput[n]
    if userinput == ispali:
        print(str(userinput) + " is a palindrome.")
        return True
    else:
        print(str(userinput)+ "is not a palindrome.")
        return False
userinput = input("Please enter a world you would like to see "
        + "if it is a palindrome: ")
is_palindrome(userinput)