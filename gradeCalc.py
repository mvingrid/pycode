'''
Ask for students grade (x3)
Calculate letter grade for average grade based on criteria bellow:
98-100… Assign the grade “A+”
95-97… Assign the grade “A”
91-94… Assign the grade “A-”
88-90… Assign the grade “B+”
84-87… Assign the grade “B”
80-83… Assign the grade “B-”
75-79… Assign the grade “C+”
70-74… Assign the grade “C”
70 and greater than 60 assign grade “D”
 If Average is less than or equal 60 assign grade "NC"
'''

#create an empty grade variable
name = input("Please enter your name: ")
allgrades = []


for i in range(1,4):
    grade = int(input("Please enter " + str(i) + " grade:" ))
    allgrades.append(grade)

averageScore = sum(allgrades)/len(allgrades)
print(averageScore)

if (averageScore >= 98) & (averageScore <= 100):
    lettergrade = "A+"
if (averageScore >= 95) & (averageScore <= 97):
    lettergrade = "A"
if (averageScore >= 91) & (averageScore <= 94):
    lettergrade = "A-"
if (averageScore >= 88) & (averageScore <= 90):
    lettergrade = "B+"
if (averageScore >= 84) & (averageScore <= 87):
    lettergrade = "B"
if (averageScore >= 80) & (averageScore <= 83):
    lettergrade = "B-"
if (averageScore >= 75) & (averageScore <= 79):
    lettergrade = "C+"
if (averageScore >= 70) & (averageScore <= 74):
    lettergrade = "C"
if (averageScore >= 61) & (averageScore <= 70):
    lettergrade = "D"
if (averageScore <= 60):
    lettergrade = "NC"

print( str(name) + " average score is:" + str(averageScore) + "\nYour average grade is " + str(lettergrade)) 
