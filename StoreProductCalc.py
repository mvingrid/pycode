'''
A consignment shop accepts a product for sale and sets an initial price.
Each month the item doesn't sell the price drops 20percent.
When the item sells the item's owner receives 60percent of the sales price, and the shop
gets 40percent.
Draw a flow chart and write pseudocode to represent the logic of program that allows the user
to enter an original product price. The output of the sale price, the owners cut, and the
shops cut for the first three months the product is on sale.
'''

month = 1
originalPrice = float(input("What is the original price of the item? "))
price = originalPrice

while(month<=3):
    sold = input("Is the item sold? YES/NO ").upper()
    ownerscut = price*0.6
    shopscut = price*0.4
    if month>1:
        price *= 0.8

    if sold == "YES":
        break
    else:
        month +=1
        continue
print("The original price is $", originalPrice)
print("After "+str(month)+ " months the item was sold for $" + str(round(price,2)))
print ("Shop cut ", round(shopscut,2))
print("Owner's cut: ", round(ownerscut,2))
