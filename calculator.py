'''
Create a calculator.
Create a function menu with options to add, subtract, multiply, or divide 
two numbers.
'''
#defined mathematical operation functions
def addition(num1,num2):
    add = num1 + num2
    return add

def subtract(num1,num2):
    sub = num1 - num2
    return sub

def multiply(num1,num2):
    mult = num1 * num2
    return mult

def divide(num1,num2):
    div = num1/num2
    return div

print("\nWelcome to calculator.py \n")
choice = 0
num1 = int(input("Please input the first number you would like to calculate: "))
num2 = int(input("\nPlease input the second number you would like to calculate: "))

while (choice != 5):
    choice = int(input("Please choose the following arithmetic option: 1 through 5 \n"
        "1) Addition\n2)Subtraction\n3)Multiplication\n4)Division\n5)Quit\n"))
    if choice == 1:
        print("Your numbers " + str(num1) + " and " + str(num2) +
        "Your total is" + str(addition(num1,num2)))
    elif choice == 2:
        print("Your numbers " + str(num1) + " and " + str(num2) +
        " Your total is" + str(subtract(num1,num2)))
    elif choice == 3:
        print("Your numbers " + str(num1) + " and " + str(num2) +
        " Your total is" + str(multiply(num1,num2)))        
    elif choice == 4:
        print("Your numbers " + str(num1) + " and " + str(num2) +
        " Your total is" + str(divide(num1,num2)))
    elif choice >= 5:
        break
