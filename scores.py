'''
Read in 10 scores from a file scores.txt
Write into another file scoreResults.txt -- LN FN MAX MIN AVG
Print as table format
'''

#open files and write file
scoresRead = open("scores.txt", 'r')
scoresWrite = open("scoreResults.txt", 'w')

line = 1
total = 0

def findAverage(newList):
    average = 0
    total = 0 # reset the value of total for new line
    for i in newList:
        total += i
    averageScore = total/len(newList)
    return averageScore

#read each line from the scores.txt file. Start at column three.
#find and print max/min/average values for each line

scoresWrite.write('LastName\tFirstName\tAverage\n')
scoresWrite.write('==========================================\n')


for i in scoresRead:
    if line == 1:
        listScores = i.split()
        LastName = listScores[0]
        FirstName = listScores[1]
        scoresWrite.write(LastName+'\t\t' + FirstName+'\t\t')
        newList = [] #create an empty array
        for j in range(2,(len(listScores)),1):
            # if I find the (total) value of line inside this loop 
            # values will keep adding to previous line
            #The new (total) value for each new line will be incorrect
            #total += float(listScores[j])
            newList.append(float(listScores[j]))
            maxScore = max(newList)
            minScore = min(newList)
            #can not find average inside loop because the total value
            #is incorrect and it will produce an incorrect value
            #averageScore = float(total/len(newList))
        scoresWrite.write(format(findAverage(newList)) +'\n')
    
scoresRead.close()
scoresWrite.close()