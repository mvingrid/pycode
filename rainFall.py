'''
Write a program that reads from a text file, a starting monthname, and ending
month name. And then a monthly rainfall for each month during that period.
As it does this, it should sum the rainfall amounts and then report the total rainfall
and average rainfall. 
'''

fileRead = open("RainFall.txt",'r')
fileWrite = open("Results.txt",'w')

line = 1
total = 0

for i in fileRead:
    if line ==3:
        list = i.split()
        for j in range(len(list)):
            total += float(list[j])
    line += 1

print(format(total/len(list), '.2f'))
fileWrite.write("The average rain fall is: \n" + format(total/len(list), '.2f'))
fileRead.close()
fileWrite.close()