'''
Description: Ask 5 users for their gender and age.
Print total number of male users who are over 25 and all females under 40 years old
'''

allgender = []
allage = []
allmaleover25 = [0]
allfemaleunder40 = [0]
males = [0]
females = [0]

for i in range(0,5):
    gender = input("Please indicate your gender, M = Male, F= Female: ").upper()
    age = int(input("Please tell us your age: "))
    if(gender == "F"):
        females[0] += 1
        if (age<= 40):
            allfemaleunder40[0] += 1
    if(gender=="M"):
        males[0] += 1
        if(age >=25):
            allmaleover25[0] += 1
print("There are " + str(females) + " females and " + str(allfemaleunder40) + " female " + " under "
    "40 years old. \nThere are " + str(males) + " males and " +str(allmaleover25) + " are over 25"
    " years old.")