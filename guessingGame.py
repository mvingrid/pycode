'''
Description: create a code to guess the
number the user inputs
'''

import random
randomval = random.randint(1,100)


def guessingGame(guessint):
    if (guessint == randomval):
        print("Correct! You guess the computer generated number")
        return True
    elif (guessint < randomval):
        print("Your guess is incorrect! Go Higher")
        return False
    elif (guessint > randomval):
        print("Your guess is Incorrect! Go Lower")
        return False

continueGame = "Yes"

while(continueGame == "Yes"):
    guessint = int(input("Please enter your best guess: "))
    guessingGame(guessint)
    continueGame = input("Would you like to continue guessing, Yes or No: ").capitalize()