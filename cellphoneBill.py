'''
You will calculate the total amount of a  cell phone bill according to:
1. Service
2. Time
Two types of servie (Regular) or (premium)

To calculate for Regulate cell service
1. Service fee is $10.00
2. Frist 50 minutes are free 
3. charges apply after using first free 50 minutes @ 0.20 per minute

To calculate for Premium cell service
1. Service fee is $25.00
2. First 75minutes are free from 6:00am - 6:00pm after using first 75 free minutes charges apply @ 0.10 per minute 
3. First 100 minutes are free from 6:00pm - 6:00am after using first free 100 minutes charges apply @ 0.05 cents per minute 

Input:
1. account number 
2. Service code
    a. Resular -->(r/R)
    b. Premium --> (p/P)
3. Number of used cell phone minutes
'''
accountNum = int(input("Please enter your account number? "))
ServiceType = str(input("What type of service do you have, Enter 'R' for regular service or 'P' for Premium: ")).upper()
charge = 0

if (ServiceType == "R"):
    minutes = int(input("Please enter the number of minutes you used: "))
    if minutes <= 50 :
        charge = 0
    else:
        charge = 0.20
    total = 10 + abs(minutes-50) *charge
    print("Your account number is " + str(accountNum) + " \nYour service type is " + str(ServiceType))
    print("Your cell phone bill is " + str(total))

elif (ServiceType == "P"):
    #combine the number of minutes used and if it was used during the day time or night time
    dayMinutes = int(input("Please enter the number of minutes you used between (6am and 6pm): "))
    nightMinutes = int(input("Pleas enter the number of minutes you used between (6pm and 6am): "))     
    if (dayMinutes <= 75 ):
        dayCharge = 0
    else:
        dayCharge = 0.10
        dayMinutesCharge = dayMinutes - 75
    if (nightMinutes <= 100):
        nightCharge = 0
    else:
        nightCharge = 0.05
        nightMinutesCharge = nightMinutes - 100
    total = 25 + (dayMinutesCharge)*dayCharge + (nightMinutesCharge)*nightCharge

    print("Your cell phone bill is " + str(total))